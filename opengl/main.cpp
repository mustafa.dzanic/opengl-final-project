﻿#include <GL/glut.h>
#include "penguin.cpp"

/*Globalne varijable korištene u cijelom programu korištene za određivanje nekih zajedničkih osobina*/

//Globalne varijable tipa bool koje nose vrijednost true ili false u zavisnosti koja tipka na mišu je kliknuta
bool leftMouseClick = GL_FALSE;
bool middleMouseClick = GL_FALSE;
bool rightMouseClick = GL_FALSE;

//Globalne varijable tipa bool koje nose vrijednost true ili false u zavisnosti koji dio prozora je kliknut (lijevi ili desni)
bool rightWindow = GL_FALSE;
bool leftWindow = GL_FALSE;

//Globalne varijable koje nose stanje true ili false za 2 izvora svjetlosti koja imamo u primjeru tj. da li je izvor upaljen ili ugašen
bool lightNumber1 = GL_FALSE;
bool lightNumber2 = GL_FALSE;

//Globalne varijable koje sadrže vrijednosti kojima se inkrementira ugao, količina translacije i ostalo
float angle;
float angleOfRotation;
float increment = 0.1;
float increment1 = 0.1;
float translation;
float scale = 0.5;
				 


void myReshape(int width, int height) {

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (width <= height)
		glOrtho(-2.0, 2.0, -2.0 * (GLfloat)height / (GLfloat)width,
			2.0 * (GLfloat)height / (GLfloat)width, -10.0, 10.0);
	else
		glOrtho(-2.0 * (GLfloat)width / (GLfloat)height,
			2.0 * (GLfloat)width / (GLfloat)height, -2.0, 2.0,
			-10.0, 10.0);

	glMatrixMode(GL_MODELVIEW);

}

//Funkcija kojom definišemo šta će biti prikazano na ekranu
void display()
{
	// Vrijednost za cuvanje handle-a nase teksture.
	static GLuint texture = 0;

	// Ovaj poziva kreira ime teksture.
	glGenTextures(1, &texture);
	glEnable(GL_TEXTURE_2D);
	// Ovaj poziv veze ime teksture kao trenutnu 2D teksturu.
	glBindTexture(GL_TEXTURE_2D, texture);

	// Specificira 2D teksturnu sliku
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, gimp_image.width, gimp_image.height,
		0, GL_RGBA, GL_UNSIGNED_BYTE, gimp_image.pixel_data);
	/*
	* Naredna dva poziva glTexParameter kazu OpenGL-u da zelimo moci "poplocati" teksturu,
	* tj. da se tekstura ponovi ako prekoracimo njene granice.
	*/
	// Postavlja parametar prelamanja za horizontalnu koordinatu, s, da se ponovi
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	// Postavlja parametar prelamanja za vertikalnu koordinatu, t, da se ponovi
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	/*
	* Naredna dva poziva glTexParameter kazu OpenGL-u da cemo koristiti GL_NEAREST
	* interpolaciju da bismo odredili koja boja je izmedju dva piksela na teksturi.
	*/
	// Postavlja parametar magnifikacije na onaj teksturni element koji je najblizi
	// centru piksela.
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Postavlja parametar minifikacije na onaj teksturni element koji je najblizi
	// centru piksela.
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// Crta cajnik na koji se primjenjuje tekstura.
	glClear(GL_COLOR_BUFFER_BIT);

	/* WireCone object*/
	glColor3f(0.0, 1.0, 1.0);	 
	glPushMatrix();
	glRotatef(0.2, 1, angleOfRotation, -0.2);
	glTranslatef(0.0, -0.6, 0.0);

	//Provjerava se koja tipka miša je stisnuta i u zavisnosti od toga translira se objekat
	if (middleMouseClick == GL_TRUE && leftWindow == GL_TRUE)
		glTranslatef(-translation, 0.0, 0.0);
	if (middleMouseClick == GL_TRUE && leftWindow == GL_FALSE)
		glTranslatef(translation, 0.0, 0.0);
	if (rightMouseClick == GL_TRUE)
		glScalef(scale, scale, scale);

	glutWireCone(0.2, 0.2, 20, 20);
	glPopMatrix();

	/* WireSphere object*/
	glColor3f(0.0, 1.0, 0.2);
	glPushMatrix();
	glRotatef(angleOfRotation, 0.0, 1.0, 0.2);
	glTranslatef(0.0, 0.6, 0.2);
	//Ako je stisnuta desna tipka, objekat se skalira
	if (rightMouseClick == GL_TRUE)
		glScalef(scale, scale, scale);
	glutWireSphere(0.2, 20, 20);
	glPopMatrix();

	/* Teapot object */
	glColor3f(1.0, 1.0, 1.0);
	glPushMatrix();
	glRotatef(0.0, 0.0, 1.0, 1.0);
	//Objekat se rotira u svakom slučaju
	glRotatef(0.0, 0.0, 1.0, 1.0);
	glutSolidTeapot(0.2);
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);


	/* Torus object 1*/

	glColor3f(1.0, 1.0, 1.0);
	glPushMatrix();
	glRotatef(-65, 1.0, 0.0, 0.0);
	glTranslatef(-0.6, 0.0, 0.0);
	glScalef(0.65, 0.65, 0.65);

	//Provjerava se da li je kliknuta desna ili lijeva tipka miša i u zavisnosti od toga objekat se rotira desno ili lijevo
	
	if (leftMouseClick == GL_TRUE && leftWindow == GL_TRUE)
		glRotatef(angleOfRotation, 0.0, 1.0, 0.0);
	if (leftMouseClick == GL_TRUE && leftWindow == GL_FALSE)
		glRotatef(-angleOfRotation, 0.0, 1.0, 0.0);

	glutWireTorus(0.05, 0.3, 25, 37);
	glPopMatrix();

	/* Torus object 2*/

	glColor3f(1.0, 1.0, 1.0);
	glPushMatrix();
	glRotatef(-65, 1.0, 0.0, 0.0);
	glTranslatef(-0.6, 0.0, 0.0);
	glScalef(0.65, 0.65, 0.65);

	//Provjerava se da li je kliknuta desna ili lijeva tipka miša i u zavisnosti od toga objekat se rotira desno ili lijevo
	
	if (leftMouseClick == GL_TRUE && leftWindow == GL_TRUE)
		glRotatef(-angleOfRotation, 0.0, 1.0, 0.0);
	if (leftMouseClick == GL_TRUE && leftWindow == GL_FALSE)
		glRotatef(angleOfRotation, 0.0, 1.0, 0.0);

	glutWireTorus(0.05, 0.3, 25, 37);
	glPopMatrix();

	/* Torus object 3*/
	glColor3f(0.5, 0.5, 1.0);
	glPushMatrix();
	glRotatef(-65, 1.0, 0.0, 0.0);
	glTranslatef(0.6, -0.2, 0.0);
	glScalef(0.65, 0.65, 0.65);

	//Provjerava se da li je kliknuta desna ili lijeva tipka miša i u zavisnosti od toga objekat se rotira desno ili lijevo
	
	if (leftMouseClick == GL_TRUE && leftWindow == GL_TRUE)
		glRotatef(angleOfRotation, 0.0, 1.0, 0.0);
	if (leftMouseClick == GL_TRUE && leftWindow == GL_FALSE)
		glRotatef(-angleOfRotation, 0.0, 1.0, 0.0);

	glutWireTorus(0.05, 0.3, 25, 37);
	glPopMatrix();

	/* Torus object 4*/
	glColor3f(0.5, 0.5, 1.0);
	glPushMatrix();
	glRotatef(-65, 1.0, 0.0, 0.0);
	glTranslatef(0.6, 0.2, 0.0);
	glScalef(0.65, 0.65, 0.65);

	//Provjerava se da li je kliknuta desna ili lijeva tipka miša i u zavisnosti od toga objekat se rotira desno ili lijevo
	
	if (leftMouseClick == GL_TRUE && leftWindow == GL_TRUE)
		glRotatef(angleOfRotation, 0.0, 1.0, 0.0);
	if (leftMouseClick == GL_TRUE && leftWindow == GL_FALSE)
		glRotatef(-angleOfRotation, 0.0, 1.0, 0.0);

	glutWireTorus(0.05, 0.3, 25, 37);
	glPopMatrix();

	glFlush();

}

//Funkcija u kojoj se postavljaju inicijalne vrijednosti
void init()
{

	glClearColor(0.0, 0.0, 0.0, 0.0);

	
	/*First light*/
	GLfloat light_position[] = { 0.75,0.75,-0.1,1.0 };
	GLfloat light_col[] = { 1.0,0.5,1.0,0.0 };

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_col);

	/*Second light*/
	GLfloat light_position1[] = { -2.0,5.0,5.0,0.0 };
	GLfloat light_col1[] = { 0.6,0.7,0.2,0.7 };
	glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_col1);

}


/*Funkcija koja definiše operacije izvođene pomoću tastature
Za svaki slučaj posebno odrađuje se određena procedura */
void kbd(unsigned char key, int x, int y)
{
	switch (key)
	{
		
	case 27:
		exit(0);
		break;
	
	case 'Q':
	case 'q':

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluLookAt(-0.30, 0.30, 0.10, 0, 0, -1, 0, 1, 0);
		glOrtho(-3, 3, 3, 3, 3, 3);
		break;

	case 'W':
	case 'w':

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluLookAt(0, 0, 0.01, 0, 0, 0, 0, 1, 0);
		glOrtho(-2, 2, -2, 2, 2, 2);
		break;

	case 'E':
	case 'e':

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluLookAt(0.30, -0.20, 0.10, 0.01, 0.01, 0.01, 0, 1, 0);
		break;

		
	case '0':
		if (lightNumber1 == GL_TRUE)
		{
			glDisable(GL_LIGHT0);
			glDisable(GL_LIGHTING);
			lightNumber1 = GL_FALSE;
		}
		else
		{
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
			lightNumber1 = GL_TRUE;
		}
		break;

	case '1':
		if (lightNumber2 == GL_TRUE)
		{
			glDisable(GL_LIGHT1);
			glDisable(GL_LIGHTING);
			lightNumber2 = GL_FALSE;
		}
		else
		{
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT1);
			lightNumber2 = GL_TRUE;
		}
		break;
}
	glutPostRedisplay();
}

/*Funckija koja se izvršava u svakom slučaju
U njoj smo definisali rotaciju srednjeg objekta bez obzira na to šta korisnik uradio

*/
void idle()
{
	if (leftMouseClick == GL_FALSE && rightMouseClick == GL_FALSE)
	{
		angleOfRotation += increment;
		if (angleOfRotation>360.0)
			angleOfRotation = 0.0;
	}

	if (leftMouseClick == GL_TRUE)
	{
		angleOfRotation += increment1;
		if (increment1 >= 0.76)
			increment1 = 0.05;
		if (angleOfRotation>360.0)
			angleOfRotation = 0.0;

	}
	
	if (middleMouseClick == GL_TRUE)
	{
		if (translation <= 0.72)
			translation += 0.001;
		else
			translation = 0;
	}

	
	if (rightMouseClick == GL_TRUE)
	{
		scale += 0.001;
		if (scale >= 1.25)
			scale = 0.25;

	}
	glutPostRedisplay();
}


void mouse(int btn, int state, int x, int y)
{
	
	if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		increment1 += 0.5;//na svaki lijevi klik povecavamo brzinu rotacije
		leftMouseClick = GL_TRUE;
	}
	if (btn == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) {
		translation += 0.3;//na svaki srednji klik povecavamo brzinu translacije
		middleMouseClick = GL_TRUE;
	}
	if (btn == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		if (leftWindow)
			scale -= 0.2;//na svaki desni klik povecavamo brzinu skaliranja
		else
			scale += 0.2;
		rightMouseClick = GL_TRUE;
	}
	

	else if (state == GLUT_UP)
	{
		leftMouseClick = GL_FALSE;
		middleMouseClick = GL_FALSE;
		rightMouseClick = GL_FALSE;
	}

	glutPostRedisplay();
}


void motion(int x, int y)
{


	if (middleMouseClick == GL_TRUE)
	{
		int width = glutGet(GLUT_WINDOW_WIDTH);
		if (x>width / 2)
			leftWindow = GL_FALSE;
		else if (x <= width / 2)
			leftWindow = GL_TRUE;
	}

	if (leftMouseClick == GL_TRUE)
	{
		int width = glutGet(GLUT_WINDOW_WIDTH);
		if (x>width / 2)
			leftWindow = GL_FALSE;
		else if (x <= width / 2)
			leftWindow = GL_TRUE;
	}

	glutPostRedisplay();
}


/*Funkcija za odredjivanje pozicije misa u odnosu na polovinu prozora*/

void passive(int x, int y)
{
	
	int width = glutGet(GLUT_WINDOW_WIDTH);
	if (x<width / 2)
		leftWindow = GL_TRUE;
	else
		leftWindow = GL_FALSE;
}




int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1000, 1000);
	glutCreateWindow("Dzanic_Mustafa_Projektni_Zadatak");
	init();
	glutDisplayFunc(display);//iscrtavanje 					 
	glutIdleFunc(idle);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutPassiveMotionFunc(passive);
	glutKeyboardFunc(kbd);
	glutMainLoop();
	return 0;
}
